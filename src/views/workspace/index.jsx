import { Layout } from 'antd';
import React, { useEffect, } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAPI } from '../../actions/fetch';
import Body from '../../containers/Body';
import Manage from '../../containers/Manage'
import Team from '../../containers/Team'
import Header from '../../containers/Header';
const Workspace = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchAPI())
    }, []);
    const current = useSelector(state => state.changeTab.tab)
    return (
        <Layout>
            <Layout.Header style={{
                backgroundColor: "rgb(28,85,150)"
            }} >
                <Header />
            </Layout.Header>
            <Layout.Content >
                {
                    current === 'workspace' ? <Body /> : current === "manage" ? <Manage /> : <Team />
                }
            </Layout.Content>
            <Layout.Footer>
            </Layout.Footer>
        </Layout>
    );
}

export default Workspace;
