import { FETCH_API, FETCH_API_SUCCESS,FETCH_API_FAIL } from "../actiontypes"
const intitialValue = {
    data: [],
    error:""
}
const fetchAPIReducer = (state = intitialValue, action) => {
    switch (action.type) {
        case FETCH_API:
            return {
                ...state,
                data:action.payload.data
            }
        case FETCH_API_SUCCESS:
            return {
                ...state,
                data:action.payload.data
            }
        case FETCH_API_FAIL:
            return {
                ...state,
                error:action.payload.error
            }
        default:
            return {
                ...state
            }
    }
}
export default fetchAPIReducer;