import { combineReducers } from "redux";
import fetchAPIReducer from "./fetchReducer";
import getPageReducer from "./getPageReducer";
import changeTabReducer from "./changeTabReducer";
import updatedDataReducer from "./updateDataReducer";
import postNewDataReducer from "./postNewDataReducer";

const rootReducer = combineReducers({
  fetchAPI: fetchAPIReducer,
  getPage: getPageReducer,
  changeTab: changeTabReducer,
  updatedData: updatedDataReducer,
  postNewData: postNewDataReducer,
});
export default rootReducer;
