import { GET_CURRENT_PAGE } from "../actiontypes";
const initialize = { current: 1 };
const getPageReducer = (state = initialize, action) => {
  switch (action.type) {
    case GET_CURRENT_PAGE:
      return {
        ...state,
        current: action.payload,
      };

    default:
      return {
        ...state
      };
  }
};
export default getPageReducer;
