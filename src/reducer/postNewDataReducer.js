import { UPDATE_NEW_DATA } from "../actiontypes";
const initialize = {
  newData: [],
};

const postNewDataReducer = (state = initialize, action) => {
  switch (action.type) {
    case UPDATE_NEW_DATA:
      return {
        ...state,
        newData: action.payload,
      };

    default:
      return { ...state };
  }
};

export default postNewDataReducer;
