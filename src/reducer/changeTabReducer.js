import { CHANGE_TAB } from "../actiontypes";
const initialize = { tab: "" }
const changeTabReducer = (state=initialize,action) => {
    switch (action.type) {
        case CHANGE_TAB:
            return {
                ...state,
                tab:action.payload
            }
        default:
            return {
                ...state
            }
    }
}
export default changeTabReducer