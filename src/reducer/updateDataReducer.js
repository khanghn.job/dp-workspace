import { UPDATE_DATA } from "../actiontypes";
const initilize = {
  updatedData: [],
};
const updatedDataReducer = (state = initilize, action) => {
  switch (action.type) {
    case UPDATE_DATA:
      return {
        ...state,
        updatedData: action.payload.data,
      };
    default:
      return { ...state };
  }
};
export default updatedDataReducer;
