import axios from "axios";
import { call, takeLatest, put } from 'redux-saga/effects'
import { fetchAPIFail, fetchAPISuccess, fetchAPI } from "../actions/fetch";
import { FETCH_API } from "../actiontypes";
import { workspaceAPI } from "../linkTo";
const getPosts = () => {
    return axios.get(workspaceAPI)
}
function* fetchPostsSaga() {
    try {
        const respone =yield call(getPosts);
        yield put(fetchAPISuccess(respone.data));
    } catch(e) {
        yield put(fetchAPIFail(e.message))
    }
}
function* fetchSaga() {
    yield takeLatest(FETCH_API, fetchPostsSaga)
}
export default fetchSaga; 