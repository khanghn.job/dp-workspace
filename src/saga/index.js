import { all} from 'redux-saga/effects'
import fetchSaga from './fetchSaga'
export default function* rootSaga(){
    yield all([
        fetchSaga(),
    ])
}
