export const FETCH_API = "FETCH_API";
export const FETCH_API_SUCCESS = "FETCH_API_SUCCESS";
export const FETCH_API_FAIL = "FETCH_API_FAIL";
export const GET_CURRENT_PAGE = "GET_CURRENT_PAGE";
export const CHANGE_TAB = "CHANGE_TAB";
export const UPDATE_DATA = "UPDATE_DATA";
export const UPDATE_NEW_DATA = "UPDATE_NEW_DATA";
