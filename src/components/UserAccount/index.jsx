import { SettingOutlined, PoweroffOutlined, CaretDownOutlined } from '@ant-design/icons';
import { Dropdown, Menu, Space } from 'antd';
import React from 'react';
import './style.scss'
const onClick = ({ key }) => {
    
};
const menu = (
    <Menu
        onClick={onClick}
        items={[
            {
                label: 'Profile',
                key: 'profile',
                icon:<SettingOutlined />
            },
            {
                label: 'Logout',
                key: 'logout',
                icon:<PoweroffOutlined />
            }
        ]}
        style={{
            width:180,
            backgroundColor: "rgb(28,85,150)"
        }} 
        theme="light"
    />
);

const UserAccount = () => (
    <Dropdown overlay={menu}>
        <span onClick={(e) => e.preventDefault()}>
            <Space>
                User account
                <CaretDownOutlined />
            </Space>
        </span>
    </Dropdown>
);

export default UserAccount;