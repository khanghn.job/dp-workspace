import { Card, Tag } from 'antd';
import React from 'react';
import './style.scss'
const { Meta } = Card;
const ItemCard = (props) => {
    const { image, description, roles,name } = props
    return (
        <Card
            hoverable
            style={{
                width: 240,
                height: 280,
                margin: 10,
                padding: 20,
                borderRadius: 10
            }}
            cover={<img src={image} alt=" " style={{ width: "100%" }} />}
        >
            <Meta
                title={name}
                style={{
                    padding: "36px 10px 24px 10px"
                }}
                description={
                    <>
                        <div>{ description}</div>
                    <Tag color={roles === 'trial'?'volcano':roles.length > 5 ? 'geekblue' : 'green'} key={roles}>
                        {roles.toUpperCase()}
                    </Tag>
                    </>
                 } />
        </Card>
    )
};

export default ItemCard;