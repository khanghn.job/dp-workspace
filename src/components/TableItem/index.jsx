import PropTypes from 'prop-types'
import React, { useState, useRef, useEffect } from 'react';
import { Layout, Divider, Table, Tag, Space, Button, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { ReloadOutlined, SearchOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';
import './style.scss'
import postNewData from '../../actions/postNewData';
const { Content } = Layout
const TableItem = (props) => {
    const { title, pageSize, hasSearchName, hasFilterRole, hasSortRole, hasSelectItem, hasAction, hasRefresh, updateData, isRef } = props
    const roles = [{ text: "TRIAL", value: "trial" }, { text: "MEMEBER", value: "member" }, { text: "ADMIN", value: "admin" }]
    const dispatch = useDispatch()
    const filterRole = hasFilterRole ? {
        filters: [...roles],
        onFilter: (value, record) => record.roles.includes(value)
    } : {}
    const sortRole = hasSortRole ? { sorter: (a, b) => a.roles.charCodeAt(0) - b.roles[0].charCodeAt(0) } : {}

    const actionDelete = hasAction ? {
        title: 'Action',
        key: 'action',
        render: (_, record) => (
            <Space size="middle">
                <Button type="link" onClick={() => handleDelete(record.key)}>Delete</Button>
            </Space>
        ),
    } : {}
    const datas = useSelector(state => state.updatedData.updatedData)
    const dataset = []
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const [selectedRow, setSelectedRow] = useState([]);
    const searchInput = useRef(null);
    datas.forEach(data => {
        dataset.push(
            {
                ...data,
                key: data.id
            }
        )
    })
    const [data, setData] = useState(dataset)
    const handleDelete = key => {
        const newData = data.filter(item => item.key !== key)
        setData(newData)
    }
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };
    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };
    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div
                style={{
                    padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Search
                    </Button>
                    <Button
                        onClick={() => clearFilters && handleReset(clearFilters)}
                        size="small"
                        style={{
                            width: 90,
                        }}
                    >
                        Reset
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => {
                            confirm({
                                closeDropdown: false,
                            });
                            setSearchText(selectedKeys[0]);
                            setSearchedColumn(dataIndex);
                        }}
                    >
                        Filter
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                    color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownOpenChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: '#ffc069',
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });
    const searchName = hasSearchName ? { ...getColumnSearchProps('name') } : {}

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            width: '20%',
            render: (text) => <a href=' ' key={Math.floor(Math.random())}>{text}</a>,
            ...searchName
        },
        {
            title: 'Created at',
            dataIndex: 'createdAt',
            key: 'created',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            width: '30%',
        },
        {
            title: 'Role',
            key: 'roles',
            dataIndex: 'roles',
            render: (roles) => {
                return (<Tag color={roles === 'trial' ? 'volcano' : roles.length > 5 ? 'geekblue' : 'green'}>
                    {roles.toUpperCase()}
                </Tag>)
            },
            ...filterRole,
            ...sortRole
        },
        actionDelete,
    ];
    console.log(selectedRow)
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelectedRow(selectedRowKeys)
        },
        getCheckboxProps: (record) => ({
            name: record.name,
            id: record.id
        }),
    };
    return (
        <>
            <Divider orientation="left">{title}</Divider>
            {hasRefresh ? < Button icon={<ReloadOutlined />} type='link' size='large' onClick={() => {
                dispatch(postNewData(data))
            }} /> : ""}
            <Content>
                <Table
                    columns={columns}
                    dataSource={(isRef && updateData.length > 0) ? updateData : data}
                    pagination={pageSize}
                    rowSelection={hasSelectItem ? rowSelection : false}
                /></Content>
        </>
    );
}
TableItem.propTypes = {
    title: PropTypes.string,
    pageSize: PropTypes.number,
    hasSearchName: PropTypes.bool,
    hasFilterRole: PropTypes.bool,
    hasSortRole: PropTypes.bool,
    hasSelectItem: PropTypes.bool,
    actionDelete: PropTypes.bool,
    hasRefresh: PropTypes.bool,
    isRef: PropTypes.bool

}
TableItem.defaultProps = {
    title: "Table",
    pageSize: 8,
    hasSearchName: false,
    hasFilterRole: false,
    hasSortRole: false,
    hasAction: false,
    hasSelectItem: false,
    hasRefresh: false,
    isRef: false
}
export default TableItem;
