import { UPDATE_DATA } from "../actiontypes";

const updateData = data => {
    return {
        type: UPDATE_DATA,
        payload: {
            data:data
        }
    }
}
export default updateData;