import { FETCH_API, FETCH_API_SUCCESS, FETCH_API_FAIL } from "../actiontypes";
export const fetchAPI = (res) => {
    return {
        type: FETCH_API,
        payload: {
            data:res
        }
    }
}
export const fetchAPISuccess = (res) => {
    return {
        type: FETCH_API_SUCCESS,
        payload: {
            data:res
        }
    }
}
export const fetchAPIFail = (error) => {
    return {
        type: FETCH_API_FAIL,
        payload: {
            error:error
        }
    }
}