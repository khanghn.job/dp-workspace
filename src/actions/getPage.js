import { GET_CURRENT_PAGE } from "../actiontypes";
const getCurrent = (current) => {
    return {
        type: GET_CURRENT_PAGE,
        payload:current
    }
}
export default getCurrent;