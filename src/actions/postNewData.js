import { UPDATE_NEW_DATA } from "../actiontypes";

const postNewData = (newdata) => {
  return {
    type: UPDATE_NEW_DATA,
    payload: newdata,
  };
};

export default postNewData;
