import { CHANGE_TAB } from "../actiontypes"
export const changeTab = (tab) => {
    return {
        type: CHANGE_TAB,
        payload:tab
    }
}