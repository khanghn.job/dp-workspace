import { Layout } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import TableItem from '../../components/TableItem';
const Manage = () => {
  const data = useSelector(state => state.postNewData.newData)
  return (
    <Layout style={{
      display: "flex",
      flexDirection: "column"
    }}
    >
      <TableItem updateData={data} isRef={true} />
      <TableItem hasSearchName={true} hasFilterRole={true} hasSortRole={true} hasAction={true} hasSelectItem={true} hasRefresh={true} />
    </Layout>
  );
}

export default Manage;
