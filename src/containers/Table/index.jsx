import React from 'react';
import { Pagination } from 'antd'
import ItemCard from '../../components/ItemCard';
import demo from '../../assets/demo-logo.png'
import { useDispatch } from 'react-redux';
import getPage from '../../actions/getPage'
const Table = (props) => {
    const { data } = props
    const dispatch = useDispatch();
    const onChange = (page) => {
        dispatch(getPage(page))
    };

    const dataset = data.slice(props.current * 10 - 10, props.current* 10)   
    return (
        <>
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    minHeight: 705,
                    justifyContent: "center"
                }}>
                {
                    dataset.map(item => {
                        return (
                            <ItemCard image={demo} description={item.description} name={item.name} roles={item.roles}  key={item.id} />
                        )
                    })
                }
            </div>
            <Pagination
                total={data.length}
                onChange={onChange}
                current={props.current}
                showSizeChanger={false}
                simple
                style={{
                    margin:"16px 0px"
                }}
            />
        </>
    );
}

export default Table;
