import React from 'react';
import { PageHeader } from 'antd';
import logo from '../../assets/logo.png'
import TabMenu from '../TabMenu';
import './style.scss'
import Notification from '../../components/Notification';
import UserAccount from '../../components/UserAccount';
const Header = () => {
    return (
        <div style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            padding: "0px 0px 0px 20px",
        }}>
            <PageHeader
                className="site-page-header"
                onBack={() => null}
                title={
                    <img src={logo} alt="DP logo" />
                }
                subTitle={
                    <TabMenu />

                }
            />
            <div style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around",
                color: "white",
                width: 250
            }} >
                <Notification />
                <UserAccount />
            </div>
        </div>
    );
}

export default Header;
