import React, { useState, useEffect } from 'react';
import { TableOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { AutoComplete, Switch, Skeleton } from 'antd';
import './style.scss'
import List from '../List';
import Table from '../Table';
import { useDispatch, useSelector } from 'react-redux';
import updateData from '../../actions/updateData'
const Body = () => {
    const dataset = []
    const [usedData, setUsedData] = useState([])
    const [isSearch, setIsSearch] = useState(false)
    const [result, setResult] = useState([])
    const datas = useSelector(state => state.fetchAPI.data ? state.fetchAPI.data : [])
    const current = useSelector(state => state.getPage.current)
    if (datas.length > 0 && usedData.length === 0) {
        datas.forEach(data => {
            dataset.push({
                ...data,
                roles: data.roles[Math.floor(Math.random() * 3)],
                img: `<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
                width="539.000000pt" height="244.000000pt" viewBox="0 0 539.000000 244.000000"
                preserveAspectRatio="xMidYMid meet">
                <metadata>
                    Created by potrace 1.16, written by Peter Selinger 2001-2019
                </metadata>
                <g transform="translate(0.000000,244.000000) scale(0.100000,-0.100000)"
                    fill="#000000" stroke="none">
                    <path d="M532 2425 c-151 -33 -302 -144 -385 -282 -43 -70 -108 -262 -128
           -373 -30 -170 -15 -484 37 -765 43 -226 196 -735 222 -735 4 0 5 -25 4 -55
           l-4 -55 64 -66 c80 -84 100 -94 187 -94 105 0 234 44 341 117 129 87 262 227
           339 357 159 268 205 463 205 861 -1 233 -12 364 -46 510 -73 316 -241 518
           -483 580 -74 19 -266 19 -353 0z m323 -187 c82 -29 125 -59 180 -129 61 -78
           101 -169 131 -298 48 -207 42 -612 -13 -856 -33 -149 -143 -354 -255 -478 -61
           -68 -174 -152 -250 -185 l-48 -21 -4 22 c-5 23 -37 385 -56 627 -18 237 -39
           657 -45 915 -7 263 -16 320 -56 349 -18 12 -15 15 43 41 107 46 261 52 373 13z
           m-601 -560 c7 -225 17 -411 42 -755 8 -122 14 -230 12 -239 -6 -32 -80 269
           -110 446 -52 311 -49 513 13 740 22 81 23 83 30 50 4 -19 10 -128 13 -242z"/>
                    <path d="M2122 1445 c-64 -18 -82 -27 -145 -72 -92 -67 -163 -180 -221 -351
           -25 -74 -41 -106 -69 -133 -33 -33 -37 -43 -37 -88 0 -41 5 -57 28 -83 21 -24
           31 -52 40 -103 19 -120 48 -198 92 -247 90 -103 216 -150 343 -130 114 18 171
           39 251 93 88 58 148 124 215 231 l48 78 7 -78 c17 -205 78 -287 197 -267 78
           13 147 113 238 343 54 136 139 304 151 297 4 -3 23 -49 40 -103 36 -111 63
           -163 114 -211 29 -28 45 -35 89 -39 99 -8 183 54 387 289 l75 86 31 -81 c47
           -125 81 -185 138 -241 55 -54 117 -88 193 -105 92 -21 256 24 350 96 17 13 34
           24 36 24 3 0 8 -20 11 -45 12 -77 70 -179 126 -220 109 -80 221 -93 333 -40
           129 60 196 211 198 440 1 99 -2 130 -20 176 -26 70 -95 140 -173 177 -41 19
           -55 31 -51 42 24 61 7 132 -37 155 -40 21 -109 19 -143 -4 -39 -25 -131 -136
           -158 -191 -13 -25 -35 -79 -49 -120 -23 -67 -31 -79 -85 -123 -111 -90 -216
           -129 -279 -103 -46 19 -80 67 -161 230 -84 169 -113 198 -203 204 -50 3 -63 0
           -103 -26 -25 -16 -104 -94 -176 -173 -72 -79 -149 -161 -171 -182 l-39 -38
           -20 43 c-11 24 -35 77 -54 118 -52 116 -95 185 -124 198 -14 7 -46 12 -70 12
           -37 0 -52 -6 -84 -36 -40 -36 -138 -175 -210 -299 l-39 -68 -7 54 c-3 30 -10
           146 -13 257 l-7 204 -37 34 c-33 29 -45 34 -88 34 -28 0 -64 -7 -80 -15 -51
           -26 -60 -58 -60 -212 l0 -138 -46 -84 c-70 -131 -132 -219 -209 -297 -78 -79
           -150 -119 -238 -134 -99 -16 -156 24 -180 126 l-14 60 80 12 c196 29 354 155
           422 337 33 88 35 218 4 282 -57 118 -206 183 -337 148z m106 -237 c32 -32 -11
           -153 -74 -207 -62 -55 -184 -102 -184 -71 0 26 64 162 95 201 56 71 133 107
           163 77z m2849 -261 c94 -51 107 -77 101 -196 -8 -133 -68 -233 -131 -217 -41
           10 -76 45 -88 89 -14 51 -13 213 1 271 15 57 32 97 39 94 3 -2 38 -20 78 -41z"/>
                </g>
            </svg>`
            })
        }
        )
        setUsedData(dataset)
    }
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(updateData(usedData))
    }, [usedData]);
    const [checked, setChecked] = useState(true)
    const [options, setOptions] = useState([]);
    const onSearch = (searchText) => {
        setIsSearch(true)
        setResult([...usedData.filter(object => object.name.includes(searchText))])
        console.log(result)
        setOptions(
            !searchText ? [] : result.forEach(item => item.name),
        );
    };

    const onSelect = (data) => {
        setUsedData(data)
    };
    return (
        <div style={{
            display: "flex",
            flexDirection: "column",
        }}>
            <div
                className='function-bar'
            >
                <AutoComplete style={{
                    width: "40%",
                    backgroundColor: "rgb(255,255,255)",
                    padding: "5px 10px",
                    fontSize: 16
                }}
                    options={options}
                    onSelect={onSelect}
                    onSearch={onSearch}
                    placeholder="Project name..." />
                <Switch checkedChildren={<TableOutlined />} unCheckedChildren={
                    <UnorderedListOutlined />
                } defaultChecked onChange={checked => setChecked(checked)} style={{
                    margin: "0px 10px"
                }} />
            </div>
            <div style={{
                backgroundColor: "white"
            }}>
                {
                    usedData ? checked ? <Table data={isSearch ? result : usedData} current={current} /> : <List data={isSearch ? result : usedData} current={current} /> : <Skeleton active />
                }
            </div>
        </div>
    )
}

export default Body;
