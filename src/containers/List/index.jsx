import React from 'react';
import { Space, Table, Tag, Button } from 'antd';
import './style.scss'
import { useDispatch } from 'react-redux';
import getPage from '../../actions/getPage'

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (text) => <a href=' ' key={Math.floor(Math.random())}>{text}</a>,
    },
    {
        title: 'Created at',
        dataIndex: 'createdAt',
        key: 'created',
    },
    {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
    },
    {
        title: 'Role',
        key: 'roles',
        dataIndex: 'roles',
        render: (roles) => {
            return (<Tag color={roles === 'trial' ? 'volcano' : roles.length > 5 ? 'geekblue' : 'green'}>
                {roles.toUpperCase()}
            </Tag>)
        }
    },
    {
        title: 'Action',
        key: 'action',
        render: (_) => (
            <Space size="middle">
                <Button type="link">Delete</Button>
            </Space>
        ),
    },
];
const List = (props) => {
    const dispatch = useDispatch();
    const onChange = (page) => {
        dispatch(getPage(page.current))
    };

    return (
        <Table columns={columns} dataSource={props.data} responsive={false} pagination={
            {
                position: ['bottomLeft'],
                simple: true,
                current: props.current
            }
        }
            onChange={onChange}/>
    );
}

export default List;
