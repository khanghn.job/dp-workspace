import React from 'react';
import { Menu } from 'antd'
import { TeamOutlined, AppstoreOutlined, ProfileOutlined } from '@ant-design/icons';
import './style.scss'
import { useDispatch } from 'react-redux';
import { changeTab } from '../../actions/changeTab';
const TabMenu = () => {
    const [current, setCurrent] = React.useState('workspace');
    const onClick = e => {
        setCurrent(e.key)
    }
    const dispatch = useDispatch()
    dispatch(changeTab(current))
    return (
        <Menu
            mode="horizontal"
            selectedKeys={[current]}
            onClick={onClick}
            style={{
                minWidth: 250,
                height:"100%"
        }}>
            <Menu.Item key="workspace" icon={<AppstoreOutlined />}>Workspace</Menu.Item>
            <Menu.Item key="manage" icon={<ProfileOutlined />} >Manage</Menu.Item>
            <Menu.Item key="team" icon={<TeamOutlined />}>Team</Menu.Item>
        </Menu>
    );
}

export default TabMenu;
