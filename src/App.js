import './App.css';
import Workspace from './views/workspace';

function App() {
  return (
    <div className="App">
      <Workspace />
    </div>
  );
}

export default App;